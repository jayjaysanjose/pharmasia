<nav class="navbar navbar-expand-md py-0 mx-0" style="background-color: #2f3689">
	<div class="navbar-nav ml-auto">
		
	</div>
	<div class="navbar-nav mr-0">
		<a class="px-2 py-3" href="#"><img src="{{asset('resources/instagram.png')}}" width="20" height="20"></a>
		<a class="px-2 py-3" href="#"><img src="{{asset('resources/facebook.png')}}" width="20" height="20"></a>
		<a class="px-2 py-3" href="#"><img src="{{asset('resources/twitter.png')}}" width="20" height="20"></a>
	</div>
</nav>

<nav class="navbar navbar-expand-md navbar-light p-0">
	<div class="container-fluid">
	    <div class="col-4">
	        <ul class="navbar-nav">
	        	<li>
	         	  	<a class="navbar-brand pl-4 pt-3" href="#">
            			<img src="{{ asset('resources/dima.png')}}">
        			</a>
        		</li>
	        </ul>
	    </div>

	    <div class="col-6">
	        <ul class="navbar-nav pt-0 mt-0">
	            <li class="navbar-nav px-5">
	                <a class="nav-link" href="#" style="color: #2f3689; font-weight: bold;">{{ __('SHOP') }}</a>  
	            </li>

	            <li class="navbar-nav">
	                <a class="nav-link" href="#" style="color: #2f3689; font-weight: bold;">{{ __('BLOG') }}</a>  
	            </li>
	        </ul>
	    </div>

	    <div class="mr-0">
	        <ul class="navbar-nav pt-0 mt-0">
	            <li class="navbar-nav">
	                <a class="nav-link" href="#"><img src="{{asset('resources/shopping-bag.png')}}" width="20" height="20"></a>
	            </li>

	            <li class="navbar-nav">
	                <a class="nav-link" href="#"><img src="{{asset('resources/profile.png')}}" width="20" height="20"></a>
	            </li>
	        </ul>
	    </div>
   	</div>
</nav>