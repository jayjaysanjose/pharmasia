<footer class="page-footer font-small px-0">
	<div class="container-fluid text-center text-md-left px-0" style="border-top: 2px solid #2f3689;">
		<div class="row pb-0 bg-white m-0" style="font-weight: 500;">
			<div class="col ml-xl-5 mt-2 pl-5 py-0 my-0">
				<ul class="nav-link list-unstyled py-0 my-0">
					<li class="pb-2"><a href="#"><img class="" src="{{asset('resources/dima.png')}}"></a></li>
					<li><a href="#">Terms and Conditions</a></li>
					<li><a href="#">Privacy Policy</a></li>
					<li><a href="#">FAQ's</a></li>
				</ul>
			</div>

			<div class="col pt-sm-1">
				<ul class="nav-link list-unstyled pb-0">
					<li class="mb-1 mt-2">MY ACCOUNT</li>
					<li><a href="#">Orders</a></li>
					<li><a href="#">Log in</a></li>
					<li><a href="#">Register</a></li>
				</ul>
			</div>

			<div class="col pt-sm-1">
				<ul class="nav-link list-unstyled pb-0">
					<li class="mb-1 mt-2">PAYMENT METHODS</li>
					<li><a href="#" src="{{ asset('resources/gray.jpg')}}" width="20" height="20"></a></li>
					<li><a href="#" src="{{ asset('resources/gray.jpg')}}" width="20" height="20"></a></li>
					<li><a href="#" src="{{ asset('resources/gray.jpg')}}" width="20" height="20"></a></li>
					<li><a href="#" src="{{ asset('resources/gray.jpg')}}" width="20" height="20"></a></li>
				</ul>		
			</div>	

			<div class="col pt-sm-1">
				<ul class="nav-link list-unstyled pb-0">
					<li class="mb-1 mt-2">DELIVERY SERVICES</li>
					<li><a href="#" src="{{ asset('resources/gray.jpg')}}" width="20" height="20"></a></li>
					<li><a href="#" src="{{ asset('resources/gray.jpg')}}" width="20" height="20"></a></li>
					<li><a href="#" src="{{ asset('resources/gray.jpg')}}" width="20" height="20"></a></li>
					<li><a href="#" src="{{ asset('resources/gray.jpg')}}" width="20" height="20"></a></li>
				</ul>		
			</div>
		</div>

		<div class="row py-2 pl-3 m-0" style="background-color: #eeded0;">
			<div class="col-xl-5 ml-xl-5 pl-5">
				<p class="mb-0">EMAIL US</p>
				<a class="alert-link" href="#">sample@gmail.com</a>
			</div>

			<div class="col-xl-5 py-0">
				<p class="mb-0">FOLLOW US</p>
				<div class="pl-2">
					<a href="#">
						<img class="" src="{{ asset('resources/instagram.png')}}" width="20" height="20">
					</a>
					<a href="#">
						<img class="" src="{{ asset('resources/facebook.png')}}" width="20" height="20">
					</a>
					<a href="#">
						<img class="" src="{{ asset('resources/twitter.png')}}" width="20" height="20">
					</a>
				</div>
			</div>	
		</div>
	</div>
</footer>