@extends('layouts.app')

@section('content')
	<div class="container-fluid p-0 color-p">
		<div class="container-fluid shadow-sm text-center height-header">
			<div class="box">
				<h1 class="display-1">Lorem ipsum!</h1>
				<label class="p-1">WELCOME  TO DIMA </label>
			</div>
		</div>

		<div class="container-fluid shadow-sm height-sub-header bg pl-0 ml-0">
			<div class="row m-0">
				<div class="col text-left">
									
				</div>

				<div class="col text-left pt-5 pr-3">
					<h1 class="display-1">Shop</h1>
					<label class="p-2">LOREM IPSUM DOLOR SIT AMET.</label>
					<p class="pt-xl-3 pr-xl-3">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>	
					<a class="btn btn-dark btn-lg btn-custom" href="#">Shop Now</a>
				</div>					
			</div>
		</div>

		<div class="container-fluid shadow-sm height-sub-header bg">
			<div class="row m-0">
				<div class="col text-left pl-xl-5 ml-xl-5 pt-5">
					<h1 class="display-1">Blog</h1>
					<label class="p-2">LOREM IPSUM DOLOR SIT AMET.</label>
					<p class="pt-xl-3 pr-xl-3 mr-0">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
					<a class="btn btn-dark btn-lg btn-custom" href="#">Read more</a>			
				</div>

				<div class="col text-left">

				</div>					
			</div>
		</div>

		<div class="container-fluid shadow-sm height-sub-header bg pl-0 ml-0">
			<div class="row m-0">
				<div class="col text-left">
									
				</div>

				<div class="col text-left pt-5 pr-3">
					<h1 class="display-1">Lorem</h1>
					<label class="p-2">LOREM IPSUM DOLOR SIT AMET.</label>
					<p class="pt-xl-3 pr-xl-5">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>	
					<a class="btn btn-dark btn-lg btn-custom" href="#">View</a>	
				</div>					
			</div>
		</div>
	</div>
@endsection